'use strict'
var soap = require('soap');
var config = require('../config');


process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"



function client(result) {
    var url = 'https://se.servicios.gob.do/se/ws/wf_ws.php?wsdl';
    soap.createClient(url, function(err, client) {
        client.setSecurity(new soap.BasicAuthSecurity(config.wsUser,config.wsPass))
        result(client)
        
        
    });
    
}




function newWorkflow(params, result) {
    
    client(data => {
        data.newWorkflow(params,  function(err, data) {
             result(data)
        });
    })
        
    
}

function newAssocDocument(params, result) {
    
    client(data => {
        data.newAssocDocument(params,  function(err, data) {
             result(data)
        });
    })
        
    
}

function editChildEntityRecord(params, result) {
    
    client(data => {
        data.editChildEntityRecord(params,  function(err, data) {
             result(data)
        });
    })
        
    
}


module.exports = {
    newWorkflow,
    newAssocDocument,
    editChildEntityRecord
}