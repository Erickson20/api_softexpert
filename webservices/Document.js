'use strict'
var soap = require('soap');
var config = require('../config');
var base64 = require('base-64');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function newDocument(params, result) {
    
    client(data => {
        data.newDocument(params,  function(err, data) {
            var IDdocumnet = data.return.split(": ")
             result(IDdocumnet[1])
        });
    })
        
    
}


function UploadFile(data, result) {
    

    var title = data.title.replace(" ", "_");
    var base64File = base64.encode(data.file);
    var item = {NMFILE: title+'.pdf', BINFILE: base64File, ERROR: ''}
    var file = {ITEM: item}
    var params = {IDDOCUMENT: data.doc, IDREVISION: '00', IDUSER: '40224190500', FILE: file}
   
    client(data => {
        data.uploadEletronicFile(params,  function(err, data) {
            result(data);
            
        });
    })
}


function setAttributeValue(params, result) {
    
    client(data => {
        data.setAttributeValue(params,  function(err, data) {
            
             result(data)
        });
    })
        
    
}

function client(result) {
    var url = 'https://se.servicios.gob.do/se/ws/dc_ws.php?wsdl';
    soap.createClient(url, function(err, client) {
        client.setSecurity(new soap.BasicAuthSecurity(config.wsUser,config.wsPass))
        result(client)
        
        
    });
    
}


module.exports = {
    newDocument,
    UploadFile,
    setAttributeValue
    }