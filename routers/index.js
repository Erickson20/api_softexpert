'use strict'
const express = require('express');
const document = require('../controllers/Document')
const workflow = require('../controllers/workflow')

const api = express.Router();



api.post('/Document/newDocument', document.newDocument);
api.post('/Document/uploadEletronicFile', document.UploadFile);
api.post('/Document/setAttributeValue', document.setAttributeValue);
api.post('/Workflow/newWorkflow', workflow.newWorkflow);
api.post('/Workflow/newAssocDocument', workflow.newAssocDocument);

module.exports = api;