'use strict'

var ws = require('../webservices/Document');

function newDocument(req, res) {
    var params = { 
        IDCATEGORY: req.body.idcategory,
        IDDOCUMET: '',
        TITLE: req.body.title,
        DSRESUMEN: req.body.dsresumen,
        ATTRIBUTES: req.body.attributes
     
    }
    ws.newDocument(params, (data) =>{
        res.send({result: data})
    })
}


function UploadFile(req, res) {
    var params = {
        title: req.body.title,
        doc: req.body.iddocument,
        file: req.body.file
    }
    ws.UploadFile(params , (data) =>{
        res.send({result: data})
    })
}

function setAttributeValue(req, res) {
    var params = { 
        IDDOCUMET:  req.body.iddocument,
        IDREVISION: req.body.idrevision,
        IDATTRIBUTE: req.body.idattribute,
        VLATTRIBUTE: req.body.vlattribute
     
    }
    ws.setAttributeValue(params, (data) =>{
        res.send({result: data})
    })
}


module.exports = {
    newDocument,
    UploadFile,
    setAttributeValue
    }