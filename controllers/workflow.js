'use strict'

var  ws = require('../webservices/workflow')




function newWorkflow(req, res) {
    var params = {
        PROCESSID: req.body.processid,
        WORKFLOWTITLE: req.body.WorkflowTitle,
        USERID: req.body.userid
    }
    ws.newWorkflow(params , (data) =>{
        res.send({result: data})
    })
}

function newAssocDocument(req, res) {
    var params = {
        WORKFLOWID: req.body.workflowid,
        ACTIVITYID: req.body.activityid,
        DOCUMENTID: req.body.documentid
    }
    ws.newAssocDocument(params , (data) =>{
        res.send({result: data})
    })
}


function editChildEntityRecord(req, res) {
    var params = {
        WORKFLOWID: req.body.workflowid,
        MAINENTITYID: req.body.mainEntityid,
        CHILDRELATIONSHIPID: req.body.childRelationshipid,
        CHILDRECORDID: req.body.childRecordoid,
        ENTITYATTRIBUTEID: req.body.entityAttributeid

    }
    ws.editChildEntityRecord(params , (data) =>{
        res.send({result: data})
    })
}





module.exports = {
    newWorkflow,
    newAssocDocument,
    editChildEntityRecord
}